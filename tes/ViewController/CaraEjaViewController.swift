//
//  viewHomeController.swift
//  MembacaBersamaEja
//
//  Created by Candra Sabdana Nugroho on 31/05/20.
//  Copyright © 2020 William Sebastian Thedja. All rights reserved.
//

import UIKit

class CaraEjaViewController: UIViewController {
    
//    @IBOutlet weak var wordGif: UIImageView!
//    @IBOutlet weak var playButton: UIButton!
   
    @IBOutlet weak var wordGif: UIImageView!
    @IBOutlet weak var playButton: UIButton!
    
    var wordImage = ""
    
    let normalButton = UIImage(named: "play")
    let stopButton = UIImage(named: "stop")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgChange()
        
        //GIF di dalam button
//        let addSubviewButton = UIImageView()
//        let playGifButton = UIImage.gif(name: "Asyik")
//        let imageGifButton = addSubviewButton
//
//        imageGifButton.animationImages = playGifButton?.images
//        imageGifButton.startAnimating()
//        playButton.setBackgroundImage(playGifButton, for: .normal)
        
    }
    
    func imgChange () {
        let tempPurple = "-ungu"
        let tempYellow = "-kuning"
        let tempRed = "-merah"
        
        if wordImage == "asyik" {
            let img = wordImage+tempPurple
            wordGif.image = UIImage(named: img)
        }
        else if wordImage == "baju" {
            let img = wordImage+tempYellow
            wordGif.image = UIImage(named: img)
        }
        else if wordImage == "ransel" {
            let img = wordImage+tempRed
            wordGif.image = UIImage(named: img)
        }
    }
    
    func animationDidFinish(anim: CAAnimation, finished flag: Bool) {
        playButton.setImage(normalButton, for: .normal)
    }
    
//    @IBAction func playButtonPressed(_ sender: Any) {
        
    @IBAction func playButtonPressed(_ sender: Any) {
        let playGifEja = "eja-"
        let playGifReal = playGifEja+wordImage
        
        let playGif = UIImage.gif(name: playGifReal)
        let imageView = wordGif

        imageView!.animationImages = playGif?.images
        imageView?.animationDuration = playGif!.duration
        imageView?.animationRepeatCount = 1
        

        playButton.isSelected = !playButton.isSelected
        if playButton.isSelected
        {
            imageView!.startAnimating()
            playButton.setImage(stopButton, for: .selected)
        } else {
            //imageView!.stopAnimating()
            playButton.setImage(normalButton, for: .selected)
        }
        
//        if playButton.image(for: .normal) == normalButton {
//            playButton.setImage(stopButton, for: .normal)
//            //playButton.isEnabled = false
//
//            imageView!.startAnimating()
//        }
//        else {
//            imageView!.stopAnimating()
//            playButton.setImage(normalButton, for: .normal)
//            playButton.isEnabled = false
//        }

    }

}
