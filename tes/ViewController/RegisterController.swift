//
//  Register.swift
//  tes
//
//  Created by afitra mamor on 04/06/20.
//  Copyright © 2020 afitra mamor. All rights reserved.
//

import Foundation

import CoreData
import UIKit

class RegisterController: UIViewController {
    
    
    @IBOutlet weak var imageLabel: UIImageView!
    
    @IBOutlet weak var inputLabel: UITextField!
    
    override func viewDidLoad() {
      super.viewDidLoad()
        
        imageLabel.image=UIImage(named: "logo-hijau")
        addPaddingAndBorder(to: inputLabel)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @IBAction func register(_ sender: Any) {
        
        Helpers().createSession(inputLabel.text ?? "", true)
         performSegue(withIdentifier: "afterRegister", sender: self)
        
    }
    
    func addPaddingAndBorder(to textfield: UITextField) {
        textfield.layer.cornerRadius =  25
        textfield.layer.borderColor = UIColor.darkGray.cgColor
        textfield.layer.borderWidth = 1.5
        let leftView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 25.0, height: 2.0))
        textfield.leftView = leftView
        textfield.leftViewMode = .always
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

}
