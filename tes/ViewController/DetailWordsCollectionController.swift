//
//  DetailWordsCollectionController.swift
//  MembacaBersamaEja
//
//  Created by Candra Sabdana Nugroho on 28/05/20.
//  Copyright © 2020 William Sebastian Thedja. All rights reserved.
//

import UIKit

class DetailWordsCollectionController: UIViewController
{
    @IBOutlet weak var wordLabel: UILabel!
    @IBOutlet weak var spellWordLabel: UILabel!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
//    @IBOutlet weak var viewHome: UIView!
//    @IBOutlet weak var viewRecord: UIView!
//    @IBOutlet weak var viewColoring: UIView!
    
    @IBOutlet weak var ColoringView: UIView!
    @IBOutlet weak var RecordView: UIView!
    @IBOutlet weak var CaraEjaView: UIView!
    
    var wordHeader = ""
    var spellWordHeader = ""
    var wordImage = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        wordLabel.text = wordHeader
        spellWordLabel.text = spellWordHeader
        
        segmentedDetails(segmentedControl!)
        
        UISegmentedControl.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor:UIColor.white], for: .selected)
        
        UISegmentedControl.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor:UIColor.black], for: .normal)

//        segmentedControl.frame.size.height = 50.0

        self.segmentedControl.layer.cornerRadius = 25.0
        
        let font = UIFont(name: "SFProRounded-Medium", size: 17)
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.font: font as Any], for: .normal)
        
    }
    
    @IBAction func segmentedDetails(_ sender: UISegmentedControl) {
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            CaraEjaView.alpha = 1
            RecordView.alpha = 0
            ColoringView.alpha = 0
        case 1:
            CaraEjaView.alpha = 0
            RecordView.alpha = 1
            ColoringView.alpha = 0
        case 2:
            CaraEjaView.alpha = 0
            RecordView.alpha = 0
            ColoringView.alpha = 1
        
        default:
            break
                
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.destination is CaraEjaViewController
        {
            if let homeView = segue.destination as? CaraEjaViewController
            {
                homeView.wordImage = wordImage
            }
        }
        
        if segue.destination is ColoringViewController
        {
            if let homeView = segue.destination as? ColoringViewController
            {
                homeView.wordImageName = wordImage
            }
        }
        
//        if segue.destination is ViewRecordController
//        {
//            if let homeView = segue.destination as? ViewRecordController
//            {
//                //homeView.wordImage = wordImage
//            }
//        }
        
        
    }
    
}

