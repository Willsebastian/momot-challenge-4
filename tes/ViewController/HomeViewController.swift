//
//  HomeViewControl.swift
//  tes
//
//  Created by afitra mamor on 27/05/20.
//  Copyright © 2020 afitra mamor. All rights reserved.
//

import Foundation
import UIKit

class HomeViewController: UIViewController {
    
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var ResultTable: UITableView!
    
    @IBOutlet weak var StoryCollection: UICollectionView!
    
    @IBOutlet weak var CategoryCollection: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        ResultTable.isHidden = true
        StoryCollection.delegate = self
        StoryCollection.dataSource = self
        
        CategoryCollection.delegate = self
        CategoryCollection.dataSource = self
        
        searchBar.delegate = self
        
        ResultTable.delegate = self
        ResultTable.dataSource = self
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func take(_ sender: Any) {
        
        StoryCollection.isHidden=true
        
    }
    
    
    func changeShow(input:Bool) ->Bool {
        if(input){
            StoryCollection.isHidden = true
            
            CategoryCollection.isHidden = true
            ResultTable.isHidden=false
        }else{
            StoryCollection.isHidden = false
            
            CategoryCollection.isHidden = false
            ResultTable.isHidden=true
        }
        return true
    }
}



extension HomeViewController: UICollectionViewDataSource,UICollectionViewDelegate{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == StoryCollection {
            return 1
        }
        else if collectionView == CategoryCollection {
            return 3
        }
        else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("touch")
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 4
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if(collectionView == StoryCollection ){
            
            let cellCostom = collectionView.dequeueReusableCell(withReuseIdentifier: "StoryHomeCell", for: indexPath) as! StoryViewCell
            
            cellCostom.photoLabel.image=UIImage(named: "Group")
            
            return cellCostom
            
        }else  {
            
            let cellCostom = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryHomeCell", for: indexPath) as! CategoryViewCell
            
            cellCostom.photoLabel.image=UIImage(named: "Group")
            //                                        cellCostom.layer.cornerRadius = 5
            cellCostom.layer.masksToBounds = true
            return cellCostom
        }
        
        
    }
    
        func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
            let sectionHeaderView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SectionHeaderHome", for: indexPath) as! SectionHeaderHome
            
            sectionHeaderView.categoryStory = "Petualangan"
            
            return sectionHeaderView
              
    }

}

extension HomeViewController :  UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if(collectionView == StoryCollection ){
            return UIEdgeInsets(top: 0, left:0, bottom: 0, right: 0)
        }else{
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let bounds = collectionView.bounds
        let heightVal = self.view.frame.height
        let widthVal = self.view.frame.width
        let cellsize = (heightVal < widthVal) ?  bounds.height/2 : bounds.width/2
        
        if(collectionView == StoryCollection ){
            return CGSize(width: cellsize + 90  , height: cellsize+0  )
        }else{
            return CGSize(width: cellsize - 1   , height:  cellsize - 80 )
        }
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

}


extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        let cellCostom = tableView.dequeueReusableCell(withIdentifier: "SearchHomeCell") as! SearchTableCell
        
        cellCostom.titleLabel.text = "ini title nya"
        cellCostom.categoryLabel.text = "ini category nya"
        cellCostom.photoLabel.image=UIImage(named: "tes-image")
        
        
        return cellCostom
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //indexint = indexPath.row
        //print(indexint)
        
        
        
    }
}
    

extension HomeViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        
        if(searchText.count>0){
            changeShow(input:true)
        }else{
            changeShow(input:false)
        }
        
        
    }
}


