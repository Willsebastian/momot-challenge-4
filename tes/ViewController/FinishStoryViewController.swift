//
//  FinishStory.swift
//  tes
//
//  Created by afitra mamor on 01/06/20.
//  Copyright © 2020 afitra mamor. All rights reserved.
//

import Foundation

import CoreData
import UIKit




class FinishStoryViewController: UIViewController {
    
    @IBOutlet weak var finishLabel: UIButton!
    
    @IBOutlet weak var photoLabel: UIImageView!
 
    var listObjek:[
    (
        photo:String,
        wordTitle: String,
        iconImage: String,
        isGif: String)] = []
    
    @IBOutlet weak var itemCollection: UICollectionView!
    var myPhoto:String = ""
    override func viewDidLoad() {
    super.viewDidLoad()
      
        finishLabel.backgroundColor = getUIColor(hex: "9961F5")
        finishLabel.layer.cornerRadius = 10
        photoLabel.image = UIImage(named:  myPhoto)
        
        itemCollection.delegate = self
        itemCollection.dataSource = self
   
//        retrieve()
       
    }
    
     
    
    func getUIColor(hex: String, alpha: Double = 1.0) -> UIColor? {
         var cleanString = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
         
         if (cleanString.hasPrefix("#")) {
             cleanString.remove(at: cleanString.startIndex)
         }
         
         if ((cleanString.count) != 6) {
             return nil
         }
         
         var rgbValue: UInt32 = 0
         Scanner(string: cleanString).scanHexInt32(&rgbValue)
         
         return UIColor(
             red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
             green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
             blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
             alpha: CGFloat(1.0)
         )
     }
     
}

extension FinishStoryViewController: UICollectionViewDataSource,UICollectionViewDelegate{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
       
       }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
   
        return listObjek.count
            
        }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
      
       
    
        let cellCostom = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemFinishCell", for: indexPath) as! ItemViewCell
 
        cellCostom.photoLabel.image = UIImage(named: listObjek[indexPath.row].photo)
                               
                               return cellCostom
                        
    
               
               
    }
    
    
    
  
    
    
}
