//
//  ViewColoringController.swift
//  MembacaBersamaEja
//
//  Created by Candra Sabdana Nugroho on 01/06/20.
//  Copyright © 2020 William Sebastian Thedja. All rights reserved.
//

import UIKit

class ColoringViewController: UIViewController {

    @IBOutlet weak var wordImage: UIImageView!
    @IBOutlet weak var redButton: UIButton!
    @IBOutlet weak var pinkButton: UIButton!
    @IBOutlet weak var yellowButton: UIButton!
    @IBOutlet weak var greenButton: UIButton!
    @IBOutlet weak var blueButton: UIButton!
    @IBOutlet weak var donkerButton: UIButton!
    @IBOutlet weak var purpleButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    
    var wordImageName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let red = "-merah"
        let realImg = wordImageName+red

        wordImage.image = UIImage(named: realImg)
        
        // Do any additional setup after loading the view.
    }
    
    func startColor() -> Bool {
        
        let allButtonCollor = [
            (
                button:redButton,
                color:"E81C24"
            ),
            (
                button:pinkButton,
                color:"E20082"
            ),
            (
                button:yellowButton,
                color:"FFE800"
            ),
            (
                button:greenButton,
                color:"009C51"
            ),
            (
                button:blueButton,
                color:"00A4E5"
            ),
            (
                button:donkerButton,
                color:"2E3188"
            ),
            (
                button:purpleButton,
                color:"9261EE"
            )
        ]
        
        for item in allButtonCollor{
            item.button?.backgroundColor = getUIColor(hex:item.color)
            item.button?.setTitle("" , for: .normal)
            item.button?.layer.cornerRadius = 15
            item.button?.layer.borderWidth = 0.3
            item.button?.layer.borderColor = UIColor.black.cgColor
        }
        
        return true
    }
    
    func getUIColor(hex: String, alpha: Double = 1.0) -> UIColor? {
        var cleanString = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cleanString.hasPrefix("#")) {
            cleanString.remove(at: cleanString.startIndex)
        }
        
        if ((cleanString.count) != 6) {
            return nil
        }
        
        var rgbValue: UInt32 = 0
        Scanner(string: cleanString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    @IBAction func redButtonDidTap(_ sender: UIButton) {
        let color = "-merah"
        let realImg = wordImageName+color
        
        wordImage.image = UIImage(named: realImg)
    }
    
    @IBAction func pinkButtonDidTap(_ sender: Any) {
        let color = "-pink"
        let realImg = wordImageName+color
        
        wordImage.image = UIImage(named: realImg)
    }
    
    @IBAction func yellowButtonDidTap(_ sender: Any) {
        let color = "-kuning"
        let realImg = wordImageName+color
        
        wordImage.image = UIImage(named: realImg)
    }
    
    @IBAction func greenButtonDidTap(_ sender: Any) {
        let color = "-hijau"
        let realImg = wordImageName+color
        
        wordImage.image = UIImage(named: realImg)
    }
    
    @IBAction func blueButtonDidTap(_ sender: Any) {
        let color = "-biru"
        let realImg = wordImageName+color
        
        wordImage.image = UIImage(named: realImg)
    }
    
    @IBAction func donkerButtonDidTap(_ sender: Any) {
        let color = "-donker"
        let realImg = wordImageName+color
        
        wordImage.image = UIImage(named: realImg)
    }
    
    @IBAction func purpleButtonDidTap(_ sender: Any) {
        let color = "-ungu"
        let realImg = wordImageName+color
        
        wordImage.image = UIImage(named: realImg)
    }
    
}
