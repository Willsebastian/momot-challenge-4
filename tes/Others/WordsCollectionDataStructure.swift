//
//  StrukturDataKoleksiKata.swift
//  MembacaBersamaEja
//
//  Created by Candra Sabdana Nugroho on 27/05/20.
//  Copyright © 2020 William Sebastian Thedja. All rights reserved.
//

import Foundation

struct  WordsCollectionCategory
{
    var alphabetCategoryTitle: String
    var wordsImageNameCollection: [String]
    var spellingWord: [String]
    var objectWord: [String]
}

class ImageLibrary
{
//    class private func generateImage(categoryWordsCollectionName: String, numberOfImage: Int) -> [String]
//    {
//        var imageNames = [String]()
//
//        for i in 1...numberOfImage {
//            imageNames.append("\(categoryWordsCollectionName)\(i)")
//        }
//
//        return imageNames
//    }
 
    
    
    class func downloadImageData() -> [String: Any]
    {
        return [
            "A": [
                "imageNames": ["asyik-default"],
                "spellWord": ["AS-YIK"],
                "objectWord": ["Asyik"]
            ],
            "B": [
                "imageNames": ["baju-default"],
                "spellWord": ["BA-JU"],
                "objectWord": ["Baju"]
            ],
//            "C": [
//                "imageNames": [""],
//                "spellWord": [""],
//                "objectWord": [""]
//            ],
//            "D": [
//                "imageNames": [""],
//                "spellWord": [""],
//                "objectWord": [""]
//            ],
//            "E": [
//                "imageNames": [""],
//                "spellWord": [""],
//                "objectWord": [""]
//            ],
//            "F": [
//                "imageNames": [""],
//                "spellWord": [""],
//                "objectWord": [""]
//            ],
//            "G": [
//                "imageNames": [""],
//                "spellWord": [""],
//                "objectWord": [""]
//            ],
//            "H": [
//                "imageNames": [""],
//                "spellWord": [""],
//                "objectWord": [""]
//            ],
//            "I": [
//                "imageNames": [""],
//                "spellWord": [""],
//                "objectWord": [""]
//            ],
//            "J": [
//                "imageNames": [""],
//                "spellWord": [""],
//                "objectWord": [""]
//            ],
//            "K": [
//                "imageNames": [""],
//                "spellWord": [""],
//                "objectWord": [""]
//            ],
//            "L": [
//                "imageNames": [""],
//                "spellWord": [""],
//                "objectWord": [""]
//            ],
//            "M": [
//                "imageNames": [""],
//                "spellWord": [""],
//                "objectWord": [""]
//            ],
//            "N": [
//                "imageNames": [""],
//                "spellWord": [""],
//                "objectWord": [""]
//            ],
//            "O": [
//                "imageNames": [""],
//                "spellWord": [""],
//                "objectWord": [""]
//            ],
//            "P": [
//                "imageNames": [""],
//                "spellWord": [""],
//                "objectWord": [""]
//            ],
//            "Q": [
//                "imageNames": [""],
//                "spellWord": [""],
//                "objectWord": [""]
//            ],
            "R": [
                "imageNames": ["ransel-default"],
                "spellWord": ["RA-N-SE-L"],
                "objectWord": ["Ransel"]
            ],
//            "S": [
//                "imageNames": [""],
//                "spellWord": [""],
//                "objectWord": [""]
//            ],
            "T": [
                "imageNames": ["tenda-default"],
                "spellWord": ["TE-N-DA"],
                "objectWord": ["Tenda"]
            ],
//            "U": [
//                "imageNames": [""],
//                "spellWord": [""],
//                "objectWord": [""]
//            ],
//            "V": [
//                "imageNames": [""],
//                "spellWord": [""],
//                "objectWord": [""]
//            ],
//            "W": [
//                "imageNames": [""],
//                "spellWord": [""],
//                "objectWord": [""]
//            ],
//            "X": [
//                "imageNames": [""],
//                "spellWord": [""],
//                "objectWord": [""]
//            ],
//            "Y": [
//                "imageNames": [""],
//                "spellWord": [""],
//                "objectWord": [""]
//            ],
//            "Z": [
//                "imageNames": [""],
//                "spellWord": [""],
//                "objectWord": [""]
//            ],
            
        ]
        
    }

    class func catchData() -> [WordsCollectionCategory]
    {
        var categories = [WordsCollectionCategory]()
        let imagesData = ImageLibrary.downloadImageData()
        let sortImagesData = imagesData.sorted {$0.key < $1.key}
        
//        print(imagesData["A"][0])
        
        
         
        
        
        
        
        
        for (categoryTitle, data) in sortImagesData
        {
            
            if let data = data as? [String: Any]
            {
                if let imagesNames = data["imageNames"] as? [String]
                {
                    if let spellWord = data["spellWord"] as? [String]
                    {
                        if let objectName = data["objectWord"] as? [String]
                        {

                            let newCategory = WordsCollectionCategory(alphabetCategoryTitle: categoryTitle, wordsImageNameCollection: imagesNames, spellingWord: spellWord, objectWord: objectName)

                            categories.append(newCategory)
                          
                        }
                    }
                }
            }
        }
        return categories
    }
}

