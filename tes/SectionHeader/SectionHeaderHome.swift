//
//  SectionHeaderHome.swift
//  tes
//
//  Created by Candra Sabdana Nugroho on 05/06/20.
//  Copyright © 2020 afitra mamor. All rights reserved.
//
import UIKit
import Foundation

class SectionHeaderHome: UICollectionReusableView {
    
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    
    var categoryStory: String! {
    didSet {
        categoryLabel.text = categoryStory
    }
}
    
}
